;;; init -- init emacs
;;;
;;; Commentary:
;;;   This is where Emacs is inited
;;;
;;; Code:

(menu-bar-mode -1)
(scroll-bar-mode -1)
(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8-unix)


(show-paren-mode 1)
(column-number-mode 1 )
(savehist-mode 1)
;(desktop-save-mode nil)

;;;Time stuff
(setq display-time-24hr-format t)
(display-time-mode 1)


(ido-mode 1)
(setq ido-enable-flex-matching t)

(setq-default indent-tabs-mode nil)

(when (display-graphic-p)
  ;; Don't suspend frame on C-z, let it undo
  (global-set-key (kbd "C-z") 'undo)
  
  ;; set a default font
  (when (member "DejaVu Sans Mono" (font-family-list))
    (set-face-attribute 'default nil :font "DejaVu Sans Mono"))

  (when (member "Liberation Mono" (font-family-list))
    (set-face-attribute 'default nil :font "Liberation Mono"))
  (when (member "OpenDyslexic" (font-family-list))
        (set-face-attribute 'variable-pitch nil :font "OpenDyslexic"))
;;  (set-face-attribute 'default nil :font "OpenDyslexic Mono")
  ;; specify font for all unicode characters
  ;; TODO: EmojiOne fonts look better in black and white but for some reason are huge
  (set-fontset-font t 'unicode (font-spec :family "Noto Emoji" ) nil 'prepend)
  (tool-bar-mode -1)

  )


(require 'package)
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)

(package-initialize)
(unless (package-installed-p 'use-package )
  (package-refresh-contents)
  (package-install 'use-package )
  )

(setq use-package-always-ensure t)

(use-package exwm
  :if (display-graphic-p)
  :after all-the-icons
  :config

  (server-start)
  
  (require 'exwm-systemtray)
  (exwm-systemtray-enable)
  (require 'exwm-randr )
  (setq exwm-randr-workspace-output-plist '(0 "VGA-1" 1 "HDMI-1" 4 "HDMI-1" ))
  (add-hook 'exwm-randr-screen-change-hook
            (lambda ()
              (let
                  ((xrandr-command "xrandr --dpi 124 --output VGA-1 --right-of HDMI-1 --auto"))
              (if
                  (file-readable-p "~/.config/autostart/lxrandr-autostart.desktop")
                  (setq xrandr-command "xdg-open ~/.config/autostart/lxrandr-autostart.desktop")
                )
              (start-process-shell-command
             "xrandr" nil xrandr-command ))
              ))
  (exwm-randr-enable)
  (require 'exwm-config)

  ;;"Default configuration of EXWM."
  ;; Set the initial workspace number.
  (setq exwm-workspace-number 4)
  ;; Make class name the buffer name
  (add-hook 'exwm-update-class-hook
          (lambda ()
            (unless (or (string-prefix-p "sun-awt-X11-" exwm-instance-name)
                        (string= "gimp" exwm-instance-name))
              (exwm-workspace-rename-buffer exwm-class-name))))

  (add-hook 'exwm-update-title-hook
            (lambda ()
              ( exwm-workspace-rename-buffer (exwm-format-window-tilte exwm-class-name exwm-title) )))

  (defun exwm-format-window-tilte (wm-class-name wm-title)
    "Formats the title giving some nice icons"
    (if (string-equal "Chromium-browser" wm-class-name)
        (setq wm-class-name (all-the-icons-faicon "chrome"))
      )
    (concat wm-class-name ":" wm-title)
    )
  
  ;; 's-r': Reset
  (exwm-input-set-key (kbd "s-r") #'exwm-reset)
  ;; 's-w': Switch workspace
  (exwm-input-set-key (kbd "s-w") #'exwm-workspace-switch)
  (exwm-input-set-key (kbd "C-s-<delete>")  #'exwm-lock)
  ;; 's-N': Switch to certain workspace
  (dotimes (i 10)
    (exwm-input-set-key (kbd (format "s-%d" i))
                        `(lambda ()
                           (interactive)
                           (exwm-workspace-switch-create ,i))))
  ;; 's-&': Launch application
  (exwm-input-set-key (kbd "s-&")
                      (lambda (command)
                        (interactive (list (read-shell-command "$ ")))
                        (start-process-shell-command command nil command)))

  ;;some short cuts ported from i3
  ;; start a terminal
  (exwm-input-set-key (kbd "s-<return>") `(lambda () (interactive) (start-process  "terminal-emulator" nil "x-terminal-emulator")))

  (exwm-input-set-key (kbd "C-s-SPC") `(lambda () (interactive) (start-process-shell-command "rofi" nil "rofi -combi-modi window,drun,run -show combi -modi combi,window,drun,run,ssh")))
  (exwm-input-set-key (kbd "s-<tab>") `(lambda () (interactive) (start-process "rofiwindow" nil "rofi" "-show" "window")))
  (exwm-input-set-key (kbd "s-SPC") #'counsel-linux-app)
  ;; kill focused window
  (exwm-input-set-key (kbd "s-Q") #'kill-this-buffer)


  ;;screenshots ( mac muscle memory )
  (exwm-input-set-key (kbd "s-#") `(lambda () (interactive) (start-process "streen-shot" nil "shutter" "-s")))
  (exwm-input-set-key (kbd "C-s-$") `(lambda () (interactive) (start-process "streen-shot" nil "shutter" "-f")))

  (add-to-list 'exwm-input-prefix-keys ?\M-o )
  ;; simulation keys are keys that exwm will send to the exwm buffer upon inputting a key combination
  (setq exwm-input-simulation-keys
   '(
     ;; movement
     ([?\C-b] . left)
     ([?\M-b] . C-left)
     ([?\C-f] . right)
     ([?\M-f] . C-right)
     ([?\C-p] . up)
     ([?\C-n] . down)
     ([?\C-a] . home)
     ([?\C-e] . end)
     ([?\M-v] . prior)
     ([?\C-v] . next)
     ([?\C-d] . delete)
     ([?\C-k] . (S-end delete))
     ;; cut/paste
     ([?\C-w] . ?\C-x)
     ([?\M-w] . ?\C-c)
     ([?\C-y] . ?\C-v)
     ;; search
     ([?\C-s] . ?\C-f)))
  (use-package exwm-edit)
  (exwm-enable))

(defun exwm-logout ()
  "Logout of lxsession."
  (interactive)
  (save-some-buffers)
  (start-process-shell-command "logout" nil "lxsession-logout"))

(defun exwm-lock ()
  "Lock the screen."
  (interactive)
  (save-some-buffers)
  (start-process-shell-command "lock" nil "xautolock -locknow || mate-screensaver-command -l || lxlock" ))


(use-package delight)
(use-package try
  :ensure t)

(use-package counsel
  :after ivy
  :defer 1
  :ensure t
  :config (counsel-mode)
  (setq counsel-linux-app-format-function 'counsel-linux-app-format-function-name-first)
  :bind ("s-SPC" . counsel-linux-app)
  :delight
  )

(use-package counsel-projectile
  :after counsel
  :ensure t
  :config (counsel-projectile-mode))

(use-package all-the-icons-ivy
  :ensure t
  :after ivy
  :defer 2
  :config
  (all-the-icons-ivy-setup)
  (add-to-list 'all-the-icons-mode-icon-alist '(exwm-mode all-the-icons-octicon "browser" :face all-the-icons-cyan) )
  )

;; (use-package spaceline
;;   :init
;;   (spaceline-emacs-theme))

(use-package doom-modeline
      :ensure t
;;      :defer f
      :hook (after-init . doom-modeline-init)
      :config
      (setq
       doom-modeline-buffer-file-name-style 'truncate-with-project
       doom-modeline-minor-modes nil)
      )
(use-package doom-themes
  :config (load-theme 'doom-city-lights t))

;; (use-package spaceline-all-the-icons
;;   :after spaceline
;;   :config
;;   :init
;;   (spaceline-all-the-icons-theme))

(use-package flx
  :ensure t)


(use-package ivy
  :defer 0.1
  :ensure t
;  :custom
;  (ivy-count-format "(%d/%d) ")
;  (ivy-display-style 'fancy)
;  (ivy-use-virtual-buffers t)
  :config (ivy-mode)
  (setq ivy-initial-inputs-alist nil)
  (setq ivy-re-builders-alist
        '((t . ivy--regex-fuzzy)))
  :delight
  )

(use-package ace-window
  :ensure t
  :bind ("M-o" . ace-window)
  :delight
  :config (ace-window-display-mode 1)
  )

(use-package wgrep)
(use-package ag)

;;; Load org mode early to ensure that the orgmode ELPA version gets picked up, not the
;;; shipped version
;;(use-package org-plus-contrib
;;  :pin org
;;  :ensure t)
(use-package org
  :ensure t
  :pin org
  :config
  ;; Fontify org-mode code blocks
  (setq org-src-fontify-natively t)

  ;;org-babel
  (setq org-confirm-babel-evaluate nil)
   ;; execute external programs.
  (org-babel-do-load-languages
   (quote org-babel-load-languages)
   (quote ((emacs-lisp . t)
	   (awk . t)
	   (calc . t)
	   (css . t)
	   (dot . t)
	   (js .t)
	   (perl . t)
	   (sed . t)
           (clojure . t)
           (gnuplot . t)
           (haskell . t)
           (latex . t)
           (octave . t)
           (org . t)
           (plantuml .t)
           (python . t)
           (ruby . t)
           (scala . t)
           (shell . t)
           (sql . t)
	   (C . t)
	   )))
  (setq org-duration-units
	`(("min" . 1)
	   ("h" . 60)
	   ("d" . ,(* 60 8))
	   ("w" . ,(* 60 8 5))
	   ("m" . ,(* 60 8 5 4))
	   ("y" . ,(* 60 8 5 4 10)))
	)
  (org-duration-set-regexps)
  (setq org-log-done 'time))
(use-package org-variable-pitch
  :ensure t)

(use-package cl
  :ensure t )

(use-package linum-relative
  :ensure t
  :init
  (linum-relative-global-mode)
  (line-number-mode nil)
  (setq linum-relative-current-symbol "") ;;gets the current line number on the current line
  :delight
  )

(use-package expand-region
  :commands ( er/expand-region er/contract-region )
  :bind  ("C-=" . er/expand-region)
  :bind  ("C--" . er/contract-region)
  :ensure t
  )

(use-package google-this
  :commands (google-this)
  :ensure t
  )

(use-package sx
  :commands (sx-search)
  :ensure
  )

(use-package multiple-cursors
  :ensure t
  :bind
  ("M-S-SPC" . mc/edit-lines )
  ( "C->" . 'mc/mark-next-like-this)
  ( "C-<" . 'mc/mark-previous-like-this)
  ( "C-c C-<" . 'mc/mark-all-like-this)
  )

(use-package smart-hungry-delete
   :bind (("<backspace>" . smart-hungry-delete-backward-char)
		 ("C-d" . smart-hungry-delete-forward-char))
   ()
   :defer nil ;; dont defer so we can add our functions to hooks
   :config (smart-hungry-delete-add-default-hooks))

(use-package magit
  :commands (magit-status)
  :bind ("C-x g" . magit-status)
  :ensure t
  :config
  (use-package magit-todos
    :ensure t)
  )

(use-package magithub
  :config (magithub-feature-autoinject t))

(use-package gitlab-ci-mode
  :ensure t
  :mode  "\\.gitlab-ci.yml\\'"
  )

(use-package gitlab-ci-mode-flycheck
  :after flycheck gitlab-ci-mode
  :ensure t
  :commands (gitlab-ci-mode)
  :init
  (gitlab-ci-mode-flycheck-enable))

(use-package flycheck
  :ensure t
  :init
  (global-flycheck-mode))

(use-package diff-hl
  :ensure t
  :config
  (global-diff-hl-mode 1)
  (diff-hl-flydiff-mode 1)
  (add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh)
  )


(use-package flyspell
  :ensure t
  :hook ((prog-mode . flyspell-prog-mode)
         (text-mode . flyspell-mode))
  :delight
  )

(use-package auto-complete
  :ensure t
  :demand
  :after fuzzy
  :config
  (ac-config-default)
  (ac-flyspell-workaround)
  )

(use-package fuzzy
  :ensure t
  :defer 0.1
  :config
  (setq ac-fuzzy-enable 1 )
  )

;; (use-package ranger
;;   :ensure
;;   :init
;;   (ranger-override-dired-mode nil)
;;   )

(use-package ac-emoji
  :ensure t
  :after auto-complete
  :defer 1
  :config (ac-emoji-setup)
  )


(use-package yasnippet
  :ensure t
  :init
  (yas-global-mode 1)
  :delight
  )

(use-package yasnippet-snippets
  :after yasnippet
  :ensure t)

(use-package elmacro
  :ensure t
  :defer 1
  :init (elmacro-mode 1)
  :delight
  )

(use-package dockerfile-mode
  :ensure t
  :mode(("\\.dockerfile\\'" . dockerfile-mode))
  )

(use-package php-mode
  :ensure t
  :mode (("\\.php\\'" . php-mode))
  :init
  (setq php-mode-coding-style (quote psr2))
  (setq php-search-documentation-browser-function 'eww-browse-url )
  (setq php-style-delete-trailing-whitespace 1)
  (if
      (file-exists-p "~/.config/composer/vendor/bin" )
      (progn
        (setq flycheck-php-phpcs-executable "~/.config/composer/vendor/bin/phpcs")
        (setq flycheck-php-phpmd-executable "~/.config/composer/vendor/bin/phpmd")
        (setq phpcbf-executable "~/.config/composer/vendor/bin/phpcbf")
        (setq phpunit-program "~/.config/composer/vendor/bin/phpunit")
        )
    )
  
  (defun php-extract-method (method-name)
    "Replace code with a method"
    (interactive "Mmethod name:")
    (kill-region (region-beginning) (region-end))
    (insert (concat "$this->" method-name "();\n" ))
    (previous-line)
    (c-indent-line )
    (php-end-of-defun)
    (insert (concat "\nprivate function " method-name "()\n{\n"))
    (yank)
    (insert "\n}")
    (previous-line)
    (c-indent-defun)
    (php-beginning-of-defun)
    )
  
  (defun my-php-mode-hook ()
    (auto-complete-mode t)
    (if (file-exists-p (setq phpcs-project-standard (concat (projectile-project-root) "phpcs.xml" )))
        (setq phpcbf-standard
              (setq flycheck-phpcs-standard phpcs-project-standard)
              )
      )
    ;;for spec files allow camel case
    (if (string-suffix-p "Spec.php" (buffer-file-name))
        (setq flycheck-phpmd-rulesets `("cleancode" "codesize" "design" "naming" "unusedcode") )
        (setq flycheck-phpmd-rulesets `("cleancode" "codesize" "controversial" "design" "naming" "unusedcode") )
        )
    (yas-global-mode 1 )
    
    (setq ac-sources  '(
			ac-source-php
			ac-source-words-in-same-mode-buffers
			ac-source-yasnippet ))
    (ac-php-core-eldoc-setup ) ;; enable eldoc
    (add-hook `projectile-idle-timer-hook `ac-php-remake-all-tags)
    )
  :hook (php-mode . my-php-mode-hook)
  :hook (php-mode . subword-mode)
                                       ;  :delight '(:eval (all-the-icons-icon-for-mode 'php-mode))
 )

(use-package ac-php
  :ensure t
  :after php-mode
  :demand
  :init (setq ac-php-auto-update-intval 180)
  :bind (
         ("C-]" . ac-php-find-symbol-at-point)
         ("C-t" . ac-php-location-stack-back)
         )
  :hook ((php-mode . ac-php-remake-tags)
         (projectile-idle-timer . ac-php-remake-tags)
         )
  )

(use-package php-refactor-mode
  :ensure
  :after php-mode
  :hook php-mode)

(use-package phpunit
  :ensure t
  :after php-mode
  :bind (
         ("C-c C-t t" . phpunit-current-test)
         ("C-c C-t c" . phpunit-current-class)
         ("C-c C-t p" . phpunit-current-project)
         )
  )

(use-package phpcbf
  :after php-mode
  :commands (phpcbf)
  :ensure t
  :hook (php-mode . phpcbf-enable-on-save)
  )

(use-package web-mode
  :ensure t
  :mode (("\\.html\\'" . web-mode)
	 ("\\.tpl\\'" . web-mode))
  )

(use-package json-mode
  :mode (("\\.json\\'" . json-mode))
  )

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init
  (setq markdown-command "multimarkdown")
  (add-hook 'markdown-mode-hook 'variable-pitch-mode)
  (setq markdown-fontify-code-blocks-natively t)
  (setq markdown-header-scaling t)
  )

(use-package ox-gfm
  :ensure t
  :commands (org-mode))

(use-package emojify
  :defer 0.5
  :init (global-emojify-mode)
  :if (display-graphic-p)
  )

(use-package wakatime-mode
  :init (global-wakatime-mode)
  :defer 0.1
  :delight '(:eval (concat "W" (all-the-icons-material "timer" :height 0.7 ) ))
  )

(use-package ob-php
  :ensure f
  :after org-mode
  :defer 10
  :init
  (add-to-list 'org-babel-load-languages '(php . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  )

(use-package ob-async
  :after org-mode )

(use-package ox-jira
  :after org-mode
  :init (setq org-export-copy-to-kill-ring 'if-interactive)
  )

(use-package org-jira
  :init (setq jiralib-url "https://gishais.atlassian.net" ))

(defun org-todo-to-jira (parent-task)
  (shell-command
   (concat "jira-cli new --project PR  --priority Medium "
           (if parent-task (concat "--type sub-task --parent  " parent-task ) " --type Task " )
           " --description \"...\"  \"" (org-get-heading 1 1 1 1 )"\"")  1 )
   )

(use-package org-sync
  :ensure)

(use-package ox-gfm
  :ensure)

(use-package projectile
  :ensure
  :config (projectile-mode 1)
  (setq projectile-project-search-path '("~/Projects"))
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  (define-key projectile-mode-map (kbd "C-c C-p") 'projectile-command-map)
  (projectile-register-project-type 'phpspec '("phpspec.yml")
				  :compile "phpspec run"
				  :test "phpspec run"
				  :test-suffix "Spec"
                                  :test-dir "spec/"
                                  :src-dir "src/")

  :delight '(:eval (concat "" (all-the-icons-octicon "repo"  :height 0.7 ) (projectile-project-name)))
  )

(use-package dumb-jump
  :ensure
  :after projectile
  )


(use-package ansi-color
  :config
  (defun colorize-compilation-buffer ()
    (toggle-read-only)
    (ansi-color-apply-on-region compilation-filter-start (point-max))
    (toggle-read-only))
  (add-hook 'compilation-filter-hook 'colorize-compilation-buffer)
  )

;; needs the atomic-chrome or ghost text package for chrome
(use-package atomic-chrome
  :ensure
  :if (display-graphic-p)
  :init
  (atomic-chrome-start-server)
  (setq atomic-chrome-buffer-open-style 'frame)
  )


(require 'epa-file)
(epa-file-enable)

;;set up some email defaults
(setq
 send-mail-function (quote mailclient-send-it)
 smtpmail-smtp-server "smtp.gmail.com"
 smtpmail-smtp-service 25
 user-mail-address "yisraeldov.gisha@gmail.com"
)
;;Some manually install projects
;;org-sync with gitlab support
;; https://github.com/To1ne/org-sync/tree/tc-gitlab-support
;;(add-to-list 'load-path "~/.emacs.d/elisp/org-sync")
;;(mapc 'load
;;      '("org-sync" "org-sync-bb" "org-sync-github" "org-sync-redmine" "org-sync-gitlab"))
(use-package org-sync
  :ensure
  :config
  (mapc 'load
        '("org-sync" "org-sync-bb" "org-sync-github" "org-sync-redmine" "org-sync-gitlab"))
  (setq org-sync-gitlab-auth-token (secrets-get-secret "Login" "gitlab-auth-token"))
  ;; you will need to set this
  )

;;mplay music with MPD (mpc)
(use-package mingus
  :commands (mingus)
  :config
  (setq  mingus-mode-always-modeline nil)
  (require 'mingus-stays-home)
  :bind
  ("s-m p" . mingus-toggle)
  ("s-m b" . mingus-browse)
  ("s-m >" . mingus-next)
  ("s-m <" . mingus-prev)
  )

(use-package command-log-mode
  :commands (command-log-mode global-command-log-mode)
  :bind
  ("C-o" . clm/open-command-log-buffer)
  )

(provide 'init)
;;; init.el ends here

;;(set-face-background 'vertical-border "#212526")
;;(set-face-foreground 'vertical-border (face-background 'vertical-border))


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ac-php-mode-line
   (quote
    (:eval
     (format "A%s"
             (ac-php-mode-line-project-status)))))
 '(ac-use-fuzzy t)
 '(column-number-mode t)
 '(compilation-environment (quote ("TERM=xterm-256color")))
 '(electric-pair-mode t)
 '(flycheck-php-phpcs-executable "~/.config/composer/vendor/bin/phpcs")
 '(flycheck-php-phpmd-executable "~/.config/composer/vendor/bin/phpmd")
 '(flycheck-phpcs-standard "~/Projects/prt-reports/phpcs.xml")
 '(fringe-mode nil nil (fringe))
 '(indicate-buffer-boundaries (quote right))
 '(indicate-empty-lines t)
 '(message-mail-alias-type (quote ecomplete))
 '(message-send-mail-function (quote smtpmail-send-it))
 '(org-bullets-bullet-list (quote ("●" "○")))
 '(org-plantuml-jar-path "~/plantuml.jar")
 '(org-taskjuggler-default-reports
   (quote
    ("textreport report \"Plan\" {
  formats html
  header '== %title =='

  center -8<-
    [#Plan Plan] | [#Resource_Allocation Resource Allocation]
    ----
    === Plan ===
    <[report id=\"plan\"]>
    ----
    === Resource Allocation ===
    <[report id=\"resourceGraph\"]>
  ->8-
}

# A traditional Gantt chart with a project overview.
taskreport plan \"\" {
  headline \"Project Plan\"
  columns bsi, name, start, end, effort,duration, complete, chart
  loadunit shortauto
  hideresource 1
}

# A graph showing resource allocation. It identifies whether each
# resource is under- or over-allocated for.
resourcereport resourceGraph \"\" {
  headline \"Resource Allocation Graph\"
  columns no, name, effort, weekly
  loadunit shortauto
  hidetask ~(isleaf() & isleaf_())
  sorttasks plan.start.up
}")))
 '(package-selected-packages
   (quote
    (ace-window emojify-logos magit-todos gitlab-ci-mode-flycheck gitlab-ci-flycheck icons-in-terminal try lua-mode yasnippet-snippets web-mode wakatime-mode w3m use-package telephone-line sx ranger projectile plantuml-mode phpunit phpcbf phpactor php-refactor-mode ox-jira ox-gfm org-sync org-plus-contrib org-mime org-jira org-bullets ob-php ob-async neotree mysql-to-org multiple-cursors mode-icons magit linum-relative image+ ialign howdoi google-this fuzzy flycheck-status-emoji exwm expand-region emojify ecb dockerfile-mode docker diff-hl bug-hunter atomic-chrome all-the-icons-dired ac-php ac-emoji)))
 '(phpcbf-executable ~/\.config/composer/vendor/bin/phpcbf t)
 '(ranger-dont-show-binary nil)
 '(ranger-show-literal nil)
 '(safe-local-variable-values
   (quote
    ((projectile-project-test-cmd . "make test")
     (flycheck-phpcs-standard
      (concat
       (projectile-project-root)
       "phpcs.xml"))
     (projectile-enable-caching)
     (explicit-shell-file-name . "docker-compose exec --user=$UID -T app bash ")
     (projectile-project-test-cmd . "docker-compose exec -T app phpunit")
     (projectile-test-cmd . "docker-compose exec -T app phpunit")
     (projectile-enable-caching . t)
     (org-src-preserve-indentation . t))))
 '(send-mail-function (quote mailclient-send-it))
 '(show-paren-mode t)
 '(show-paren-style (quote mixed))
 '(smtpmail-smtp-server "smtp.gmail.com" t)
 '(smtpmail-smtp-service 25 t)
 '(tool-bar-mode nil)
 '(wakatime-cli-path "/usr/local/bin/wakatime")
 '(wakatime-python-bin nil))


(put 'erase-buffer 'disabled nil)
