(let
    ((yt-series
      "Emacs Is Great");;
     (ep "45") (icons "🐍") 
     (sub-title
"Python Part 2 - Anaconda")

     ;; ------------------------------------------------------
     (yt-tags '(
		"emacs"
		"emacs-lisp"
		;;------
		"Gitlab"
		"programming"
		"org-mode"
                "packagemangement"
                "expand-region"
		"emacs lisp"
		"codding"
		))
     yt-title yt-file yt-description)
  (setq yt-title (format "%s - Ep %d, %s" yt-series (string-to-number ep) sub-title))
  (setq yt-description yt-title )
  (setq yt-file (car(last (directory-files "~/Videos" 1 "^[0-9]+.+\\(mkv\\|flv\\)" ))))
  (async-shell-command (format "youtube-upload --title \"%s\" --playlist \"%s\"  --tags=\"%s\" -d \"yt-description\" --open-link  \"%s\" "
			       yt-title
			       yt-series
			       (string-join yt-tags ", " )
			       yt-file
			       ) "*youtube-upload*")
  )
;; Local Variables:
;; eval: (face-remap-add-relative 'default  :height 2.0 )
;; eval: (face-remap-add-relative 'font-lock-string-face :foreground "black" :weight 'ultra-bold  :height 3.0)
;; eval: (face-remap-add-relative 'font-lock-string-face :background (car (sort (copy-list x-colors) '(lambda (x y) (< (random) (random) )))))
;; eval: (buffer-face-mode 0)
;; End:
